package modelo;

import java.awt.Color;

public class Carro {
	public Modelo modelo;
	public Color cor;
	public Marca marca;

	public void exibeInformacoes() {
		System.out.println("marca = " + this.marca.nome);
		System.out.println("modelo = " + this.modelo.nome + ", " 
		+ this.modelo.motor);
		System.out.println("cor = " + this.cor);
	}
}
