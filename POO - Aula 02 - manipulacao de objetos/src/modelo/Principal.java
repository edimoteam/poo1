package modelo;

import java.awt.Color;

public class Principal {
	public static void main(String[] args) {
		Carro carro1 = new Carro();
		Modelo modelo1 = new Modelo();
		Marca marca1 = new Marca();
		modelo1.motor = 1.6f;
		modelo1.nome = "HB20";
		marca1.nome = "Hyundai";
		carro1.modelo = modelo1;
		carro1.marca = marca1;
		carro1.cor = Color.blue;
		//carro1.marca.nome = "hyundai";
		 carro1.exibeInformacoes();
		//System.out.println(carro1.marca.nome);
	}
}
