package construtores;

public class PessoaFisica {
	public String nome;
	public int cpf;
	public float saldo;

	public PessoaFisica(String nome, int cpf) {
		this.nome = nome;
		this.cpf = cpf;
		this.saldo = -100;
	}

	public void realizarDeposito(float valor) {
		this.saldo += valor;
	}

	public String getInformacoes() {
		return "Nome: " + this.nome + ", cpf: " + this.cpf + ", saldo: " + this.saldo;
	}
}
