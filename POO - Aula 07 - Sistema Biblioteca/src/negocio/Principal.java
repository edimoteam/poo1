package negocio;

import java.util.Scanner;

import modelo.Aluno;
import modelo.Biblioteca;
import modelo.Livro;

public class Principal {
	public static void main(String[] args) {
		Biblioteca biblioteca = new Biblioteca();
		Scanner leitor = new Scanner(System.in);
		boolean deveContinuar = true;
		while (deveContinuar) {
			System.out.println("1) Cadastrar livro");
			System.out.println("2) Exibir livros");
			System.out.println("3) Buscar livro");
			System.out.println("4) Excluir livro");
			System.out.println("5) Cadastrar aluno");
			
			System.out.println("9) Sair");

			switch (leitor.nextInt()) {
			case 1:
				Livro livro = new Livro();
				livro.criarLivro();
				biblioteca.cadastrarLivro(livro);
				break;
			case 2:
				biblioteca.exibirLivros();
				break;
			case 3:
				biblioteca.buscarLivro();
				break;
			case 4:
				biblioteca.excluirLivro();
				break;
			case 5:
				Aluno aluno = new Aluno();
				aluno.criarAluno();
				biblioteca.cadastrarAluno(aluno);
				break;
			case 6:
				Aluno alunoEncontrado = biblioteca.buscarAluno();
				Livro livroEncontrado = biblioteca.buscarLivro();
				biblioteca.locarLivro(alunoEncontrado,livroEncontrado);
				break;
			case 9:
				deveContinuar = false;
				break;
			default:
				break;
			}
		}
	}
}
