package modelo;

import java.util.Scanner;

public class Livro {
	private String titulo;
	private String autor;
	private long isbn;
	private Aluno alunoQueAlugou;
	private boolean isLocado = false;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getInformacoes() {
		String titulo = "Titulo = " + getTitulo();
		String autor = "Autor = " + getAutor();
		String isbn = "ISBN = " + getIsbn();
		String isLocado = "Locado =" + isLocado();
		String separador = ", ";
		String informacoes = titulo + separador + autor + separador 
				+ isbn + separador + isLocado;
		return informacoes;
	}

	public void criarLivro() {
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o titulo do livro: ");
		setTitulo(leitor.next());
		System.out.println("Digite o autor do livro: ");
		setAutor(leitor.next());
		System.out.println("Digite o ISBN do livro: ");
		setIsbn(leitor.nextLong());

	}

	public void exibirInformacoes() {
		System.out.println(getInformacoes());
	}

	public boolean isLocado() {
		return isLocado;
	}

	public void setLocado(boolean isLocado) {
		this.isLocado = isLocado;
	}

	public Aluno getAlunoQueAlugou() {
		return alunoQueAlugou;
	}

	public void setAlunoQueAlugou(Aluno alunoQueAlugou) {
		this.alunoQueAlugou = alunoQueAlugou;
	}
}
