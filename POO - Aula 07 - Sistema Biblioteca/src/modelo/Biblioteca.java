package modelo;

import java.util.ArrayList;
import java.util.Scanner;

public class Biblioteca {
	private ArrayList<Livro> livrosList = new ArrayList<Livro>();
	private ArrayList<Aluno> alunosList = new ArrayList<Aluno>();

	public ArrayList<Livro> getLivrosList() {
		return livrosList;
	}

	public void setLivrosList(ArrayList<Livro> livrosList) {
		this.livrosList = livrosList;
	}

	public ArrayList<Aluno> getAlunosList() {
		return alunosList;
	}

	public void setAlunosList(ArrayList<Aluno> alunosList) {
		this.alunosList = alunosList;
	}

	public void cadastrarLivro(Livro livro) {
		getLivrosList().add(livro);
	}

	public void cadastrarAluno(Aluno aluno) {
		getAlunosList().add(aluno);
	}

	public void exibirLivros() {
		for (Livro livro : getLivrosList()) {
			livro.exibirInformacoes();
		}
	}

	public Livro buscarLivro() {
		Livro livroEncontrado = null;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o titulo do livro");
		String titulo = leitor.next();
		boolean achou = false;
		for (Livro livro : getLivrosList()) {
			if (livro.getTitulo().equals(titulo)) {
				livro.exibirInformacoes();
				achou = true;
				livroEncontrado=livro;
			}
		}
		if (!achou) {
			System.out.println("Não encontramos esse livro");
		}
		return livroEncontrado;
	}

	public Aluno buscarAluno() {
		Aluno alunoEncontrado = null;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o nome do aluno");
		String nome = leitor.next();
		boolean achou = false;
		for (Aluno aluno : getAlunosList()) {
			if (aluno.getNome().equals(nome)) {
				achou = true;
				alunoEncontrado = aluno;
			}
		}
		if (!achou) {
			System.out.println("Não encontramos esse aluno");
		}
		return alunoEncontrado;
	}

	public void excluirLivro() {
		Scanner leitor = new Scanner(System.in);
		Livro livroEncontrado = null;
		System.out.println("Digite o titulo do livro");
		String titulo = leitor.next();
		boolean achou = false;
		for (Livro livro : getLivrosList()) {
			if (livro.getTitulo().equals(titulo)) {
				livroEncontrado = livro;
				achou = true;
			}
		}
		if (achou) {
			getLivrosList().remove(livroEncontrado);
		} else {
			System.out.println("Não encontramos esse livro");
		}
	}

	public void locarLivro(Aluno aluno, Livro livro) {
		if(livro.isLocado()) {
			System.out.println("Esle livro já está locado");
		}else {
			livro.setLocado(true);
			livro.setAlunoQueAlugou(aluno);
		}
	}

}
