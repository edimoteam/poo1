package modelo;

import java.util.Scanner;

public class Aluno {
	private String nome;
	private long matricula;
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getMatricula() {
		return matricula;
	}

	public void setMatricula(long matricula) {
		this.matricula = matricula;
	}

	public void criarAluno() {
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o nome do aluno: ");
		setNome(leitor.next());
		System.out.println("Digite a matricula do aluno: ");
		setMatricula(leitor.nextLong());

	}
}
