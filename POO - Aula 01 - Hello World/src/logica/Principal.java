package logica;

import modelo.Pessoa;

public class Principal {
	public static void main(String[] args) {
		float imc;
		System.out.println("Hello World!");
		Pessoa pessoa1 = new Pessoa();
		Pessoa pessoa2 = new Pessoa();
		pessoa1.nome = "pedro";
		pessoa1.idade = 25;
		pessoa1.altura = 1.87f;
		pessoa1.peso = 76.3f;
		pessoa1.correr();
		pessoa1.falar();
		imc = pessoa1.calcularImc();
		System.out.println(imc);
		pessoa2.nome = "Maria";
		pessoa2.idade = 27;
		pessoa2.altura = 1.73f;
		pessoa2.peso = 59.8f;
		pessoa2.correr();
		pessoa2.falar();
		imc = pessoa2.calcularImc();
		System.out.println(imc);
		pessoa1.falarComPessoa(pessoa2);
	}
}
