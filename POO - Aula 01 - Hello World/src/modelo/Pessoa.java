package modelo;

public class Pessoa {
	public String nome;
	public float peso, altura;
	public int idade;

	public void correr() {
		System.out.println("Eu " + nome + " estou correndo");
	}

	public void falar() {
		System.out.println("Eu " + nome + " estou falando");
	}

	public void falarComPessoa(Pessoa pessoa) {
		System.out.println("Eu " + nome + " estou falando com " + pessoa.nome);
	}

	public float calcularImc() {
		float imc = peso / (altura * altura);
		return imc;
	}

}
