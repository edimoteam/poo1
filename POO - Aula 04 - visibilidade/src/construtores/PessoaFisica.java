package construtores;

public class PessoaFisica {
	private String nome;
	private int cpf;
	private float saldo;

	public PessoaFisica() {

	}

	public PessoaFisica(String nome, int cpf) {
		this.nome = nome;
		this.cpf = cpf;
		this.saldo = 0.0f;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public String getNome() {
		if (this.nome != null) {
			return this.nome;
		}else {
			return "O nome ainda não foi preenchido de forma adequadada";
		}
	}

	public void setNome(String nome) {
		if (nome.equals("")) {
			System.out.println("Nome invalido, tente outra vez");
		} else {
			this.nome = nome;
		}
	}

	public int getCpf() {
		return this.cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

	public void realizarDeposito(float valor) {
		this.saldo += valor;
	}

	public String getInformacoes() {
		return "Nome: " + this.nome + ", cpf: " + this.cpf + ", saldo: " + this.saldo;
	}

}
