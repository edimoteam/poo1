package visibilidade;

import construtores.PessoaFisica;

public class Principal {
	public static void main(String[] args) {
		PessoaFisica pessoaFisica1 = new PessoaFisica("Edimo", 003215);
		PessoaFisica pessoaFisica2 = new PessoaFisica();
		pessoaFisica2.setNome("");
		pessoaFisica2.setCpf(64534);
		pessoaFisica2.setSaldo(-9999999);
		System.out.println(pessoaFisica2.getNome());
		System.out.println(pessoaFisica2.getCpf());
		System.out.println(pessoaFisica2.getSaldo());
		System.out.println(pessoaFisica1.getInformacoes());
		System.out.println(pessoaFisica2.getInformacoes());

	}
}
