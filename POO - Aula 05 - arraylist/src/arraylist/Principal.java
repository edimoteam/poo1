package arraylist;

import java.util.ArrayList;
import java.util.Collections;

public class Principal {
	public static void main(String[] args) {
		ArrayList<String> nomes1 = new ArrayList<String>();
		nomes1.add("Marcos");
		nomes1.add("Douglas");
		// 3= criar uma segunda lista de nomes;
		ArrayList<String> nomes2 = new ArrayList<String>();
		nomes2.add("Bel");
		nomes2.add("Thierry");
		// 1= percorrer a lista de nomes usando o foreach;
		System.out.println("lista 1");
		for (String a : nomes1) {
			System.out.println(a);
		}// 2= percorrer a lista de nome usando o for com o size;
		System.out.println("lista 2");
		for (int i = 0; i < nomes2.size(); i++) {
			System.out.println(nomes2.get(i));
		}// 4= unir as duas listas usando addAll;
		nomes1.addAll(nomes2);
		System.out.println("lista 3");
		for (String nome : nomes1) {
			System.out.println(nome);
		} // 1 criar uma lista e preencher com objetos da classe Pessoa
		  // 2 criar uma lista e preencher com objetos da classe Carro
		  // 3 criar uma lista de Object e adicionar as pessoas e os carros
	}
}
