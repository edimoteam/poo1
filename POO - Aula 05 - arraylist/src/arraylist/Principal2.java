package arraylist;

import java.util.ArrayList;

public class Principal2 {
	public static void main(String[] args) {
		ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
		pessoas.add(new Pessoa("Edimo"));
		pessoas.add(new Pessoa("Roberto"));

		ArrayList<Carro> carros = new ArrayList<Carro>();
		carros.add(new Carro("Corola"));
		carros.add(new Carro("HB 20"));

		ArrayList<Object> objetos = new ArrayList<Object>();
		objetos.addAll(pessoas);
		objetos.addAll(carros);

		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa.getNome());
		}

		for (Carro carro : carros) {
			System.out.println(carro.getModelo());
		}
		for (Object object : objetos) {
			System.out.println(object);
		}
	}
}
